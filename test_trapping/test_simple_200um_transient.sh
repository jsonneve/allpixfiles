for model in ljubljana cmstracker
do
    for fluence in {0..5} 15 30 130
        do
            export fluence1=$fluence
            export fluence2=$(($fluence + 6))
            #echo ./jory/bin/allpix "-c allpixfiles/test_trapping/trapping_example_diode_transient.conf -o" TransientPropagation.detector2.trapping_model=$model "-o" TransientPropagation.detector1.trapping_model=$model "-o" TransientPropagation:detector1.fluence=${fluence1}e13 "-o" TransientPropagation:detector2.fluence=${fluence2}e13 "-o" root_file=trapping_example_200um_${model}_${fluence1}e13_${fluence2}e13_transient.root "-o" number_of_events=3000
            #./jory/bin/allpix -c allpixfiles/test_trapping/trapping_example_diode_transient.conf -o TransientPropagation.detector2.trapping_model=$model -o TransientPropagation.detector1.trapping_model=$model "-o" TransientPropagation:detector1.fluence=${fluence1}e13 "-o" TransientPropagation:detector2.fluence=${fluence2}e13 "-o" root_file=trapping_example_200um_${model}_${fluence1}e13_${fluence2}e13_transient.root "-o" number_of_events=10000
            #./jory/bin/allpix -c allpixfiles/test_trapping/trapping_example_diode_transient.conf -o trapping_model=$model -o transientPropagation:detector1.fluence=${fluence1}e13 -o transientPropagation:detector2.fluence=${fluence2}e13 -o root_file=trapping_example_200um_${model}_${fluence1}e13_${fluence2}e13_transient.root -o number_of_events=10000
            #echo ./jory/bin/allpix "-c allpixfiles/test_trapping/trapping_example_diode_transient.conf -o" trapping_model=$model "-o" transientPropagation:detector1.fluence=${fluence1}e14 "-o" transientPropagation:detector2.fluence=${fluence2}e14 "-o" root_file=trapping_example_200um_${model}_${fluence1}e14_${fluence2}e14_transient.root "-o" number_of_events=10000
            echo ./jory/bin/allpix "-c allpixfiles/test_trapping/trapping_example_diode_transient.conf -o" TransientPropagation.detector2.trapping_model=$model "-o" TransientPropagation.detector1.trapping_model=$model "-o" TransientPropagation:detector1.fluence=${fluence1}e14 "-o" TransientPropagation:detector2.fluence=${fluence2}e14 "-o" root_file=trapping_example_200um_${model}_${fluence1}e14_${fluence2}e14_transient.root "-o" number_of_events=3000
            #./jory/bin/allpix -c allpixfiles/test_trapping/trapping_example_diode_transient.conf -o trapping_model=$model -o transientPropagation:detector1.fluence=${fluence1}e14 -o transientPropagation:detector2.fluence=${fluence2}e14 -o root_file=trapping_example_200um_${model}_${fluence1}e14_${fluence2}e14_transient.root -o number_of_events=10000
        done
done

#./jory/bin/allpix -c allpixfiles/test_trapping/trapping_example_diode_transient.conf -o root_file=trapping_example_200um_cmstracker_1e13_1e14_transient_ModelCMStracker_fluencecorr_efield_600V_simon.root -o number_of_events=10000
