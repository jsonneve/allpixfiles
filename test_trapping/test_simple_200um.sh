for model in ljubljana cmstracker
do
    for fluence in {0..5} 15 30 130
        do
            export fluence1=$fluence
            export fluence2=$(($fluence + 6))
            echo ./jory/bin/allpix "-c allpixfiles/test_trapping/trapping_example_diode.conf -o" trapping_model=$model "-o" GenericPropagation:detector1.fluence=${fluence1}e13 "-o" GenericPropagation:detector2.fluence=${fluence2}e13 "-o" root_file=trapping_example_200um_${model}_${fluence1}e13_${fluence2}e13_generic.root "-o" number_of_events=10000
            ./jory/bin/allpix -c allpixfiles/test_trapping/trapping_example_diode.conf -o trapping_model=$model -o GenericPropagation:detector1.fluence=${fluence1}e13 -o GenericPropagation:detector2.fluence=${fluence2}e13 -o root_file=trapping_example_200um_${model}_${fluence1}e13_${fluence2}e13_generic.root -o number_of_events=10000
            echo ./jory/bin/allpix "-c allpixfiles/test_trapping/trapping_example_diode.conf -o" trapping_model=$model "-o" GenericPropagation:detector1.fluence=${fluence1}e14 "-o" GenericPropagation:detector2.fluence=${fluence2}e14 "-o" root_file=trapping_example_200um_${model}_${fluence1}e14_${fluence2}e14_generic.root "-o" number_of_events=10000
            ./jory/bin/allpix -c allpixfiles/test_trapping/trapping_example_diode.conf -o trapping_model=$model -o GenericPropagation:detector1.fluence=${fluence1}e14 -o GenericPropagation:detector2.fluence=${fluence2}e14 -o root_file=trapping_example_200um_${model}_${fluence1}e14_${fluence2}e14_generic.root -o number_of_events=10000
        done
done