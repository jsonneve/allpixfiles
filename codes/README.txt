Hi Jory,

Here is the ATHENA code:

https://gitlab.cern.ch/atlas/athena/-/blob/master/InnerDetector/InDetDigitization/PixelDigitization/src/SensorSimPlanarTool.cxx

For Allpix v1.0, the most updated versions are here:

https://github.com/ALLPix/allpix/blob/RadDamage-dev/src/AllPixFEI4RadDamageDigitizer.cc

And here:

https://github.com/ALLPix/allpix/blob/RadDamage-dev/src/AllPixFEI4RadDamage3DDigitizer.cc

> I have a few questions:
> Do I understand it correctly that you implemented not only whether a
> charge is trapped or not, but also the induced charge according to the
> altered Ramo potential after irradiation (? I assume this was in
> TCAD?) even when the charge is trapped?

Yes, exactly.

> And what does doDrift do? Is there not always drift?

In the Allpix code, we have many physics switches to turn off some effects that were used to help debug.  Their default settings should all be to be off so that the most physical result can be used.

Please let me know if you have any other questions!

Sincerely,
Ben