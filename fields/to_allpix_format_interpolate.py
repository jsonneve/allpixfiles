#!/usr/bin/env python3

import sys
import decimal
from scipy.interpolate import griddata
import numpy as np

if __name__ == "__main__":

    bins_x = 20
    bins_y = 20
    bins_z = 200
    temperature = 253

    grid_x, grid_z = np.mgrid[0:5000:20j, 0:200:200j]
    if len(sys.argv) < 2:
        print("Usage: ")
        print("\t python to_allpix_format.py /eos/path/to/efield.txt [outputfile]")
        sys.exit()
    in_file = sys.argv[1]
    if len(sys.argv) > 2:
        outfile = sys.argv[2]
    else:
        outfile = in_file.replace('/', '_')[:-4] + '.init'
    z_zfield = {}
    z_keys = []
    z_fields = []
    string = ''
    for line in open(in_file, 'r').readlines():
        if line == '' or len(line.split()) < 2 or line.startswith('#'):
            string = line
            continue
        zpoint, zfield = line.split()
        zfield = '-' + zfield # it is not negative but should be
        #z_zfield[decimal.Decimal(z)] = decimal.Decimal(zfield)
        z_zfield[zpoint] = zfield
        z_keys.append(zpoint)
        z_fields.append(zfield)
    points = []
    values = []
    for x in range(bins_x):
        for z in z_keys:
            points.append((x, z))
            values.append(z_zfield[z])
    grid_xz = griddata(points, values, (grid_x, grid_z), method='cubic')
    print(grid_xz)
    print(grid_xz.shape)

    thickness = 200
    pitch_x = 5000
    pitch_y = 5000
    header = string + '\n'
    header += "##SEED##  ##EVENTS##\n"
    header += "##TURN## ##TILT## 1.0\n"
    header += "0.00 0.0 0.00\n"
    header += str(thickness) + '. '
    header += str(pitch_x) + '. '
    header += str(pitch_y) + '. '
    header += str(temperature) + '. '
    header += "0.0 1.12 1 "
    header += str(bins_x) + ' '
    header += str(bins_y) + ' '
    header += str(bins_z) + ' '
    header += '0\n'
    print("number of bins: " + str(len(z_keys)))
    print("number of keys: " + str(len(z_zfield.keys())))
    with open(outfile, 'w') as efieldfile:
        efieldfile.write(header)
        for binz in range(1,bins_z + 1):
            for binx in range(1, bins_x + 1):
                for biny in range(1, bins_y + 1):
                    line = str(binx) + '   ' # bin x
                    line += str(biny) + '   ' # bin y
                    line += str(int(binz)).ljust(4) # bin z
                    line += '1e-9   ' # efield in x
                    line += '1e-9   ' # efield in y
                    #line += str(z_keys[znumber])
                    line += str(grid_xz[binx - 1, binz - 1]).ljust(16) # efield in z
                    line += '\n'
                    efieldfile.write(line)


    print("wrote to file " + outfile)




