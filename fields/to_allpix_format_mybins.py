#!/usr/bin/env python3

import sys
import decimal

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage: ")
        print("\t python to_allpix_format.py /eos/path/to/efield.txt [outputfile]")
        sys.exit()
    reverse = False
    in_file = sys.argv[1]
    if len(sys.argv) > 2:
        outfile = sys.argv[2]
    else:
        outfile = in_file.replace('/', '_')[:-4] + '.init'
    #input("writing to outputfile: " + outfile + ". OK?\n")
    z_zfield = {}
    z_keys = []
    z_fields = []
    for line in open(in_file, 'r').readlines():
        z, zfield = line.split()
        zfield = '-' + zfield # it is not negative but should be
        #z_zfield[decimal.Decimal(z)] = decimal.Decimal(zfield)
        z_zfield[z] = zfield
        z_keys.append(z)
        z_fields.append(zfield)
        lastz = z
        last_zfield = zfield
    temperature = 253
    pitch_x = 5000 # um
    pitch_y = 5000 # um
    thickness = 200 # um
    pitch_y = int(y[y.index('_') + 1 :])
    if pitch_y == 0:
        pitch_y = int(y[-3:])
    # in allpix2 the model is 100x25:
    if pitch_x == 25: # swap pitches
        pitch_x = pitch_y
        pitch_y = 25
    string = in_file
    bins_x = 20
    bins_y = 20
    bins_z = len(list(z_zfield.keys()))
    # FIXME bug in allpix2?
    #if bins_z < 100: # 99
    #    extra_bin = True
    #    bins_z += 1
    #    z_keys.append(100)
    #    z_zfield[100] = last_zfield
    #    z_fields.append(last_zfield)
    header = string + '\n'
    header += "##SEED##  ##EVENTS##\n"
    header += "##TURN## ##TILT## 1.0\n"
    header += "0.00 0.0 0.00\n"
    header += str(thickness) + '. '
    header += str(pitch_x) + '. '
    header += str(pitch_y) + '. '
    header += str(temperature) + '. '
    header += "0.0 1.12 1 "
    header += str(bins_x) + ' '
    header += str(bins_y) + ' '
    header += str(bins_z) + ' '
    header += '0\n'
    print("number of bins: " + str(len(z_keys)))
    print("number of keys: " + str(len(z_zfield.keys())))
    with open(outfile, 'w') as efieldfile:
        efieldfile.write(header)
        for znumber in range(len(z_keys)):
            for binx in range(1, bins_x + 1):
                for biny in range(1, bins_y + 1):
                    line = str(binx) + '   ' # bin x
                    line += str(biny) + '   ' # bin y
                    line += str(int(float(z_keys[znumber]))).ljust(4) # bin z
                    line += '1e-9   ' # efield in x
                    line += '1e-9   ' # efield in y
                    #line += str(z_keys[znumber])
                    line += str(z_fields[znumber]).ljust(16) # efield in z
                    #line += str(z_fields[znumber]).ljust(16) # efield in z
                    line += '\n'
                    efieldfile.write(line)


    print("wrote to file " + outfile)




