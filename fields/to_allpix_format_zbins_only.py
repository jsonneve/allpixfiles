#!/usr/bin/env python3

import sys
import decimal

if __name__ == "__main__":
    temperature = 253
    pitch_x = 5000
    pitch_y = 5000
    thickness = 200
    if len(sys.argv) < 2:
        print("Usage: ")
        print("\t python to_allpix_format.py /eos/path/to/efield.txt [outputfile]")
        sys.exit()
    in_file = sys.argv[1]
    if len(sys.argv) > 2:
        outfile = sys.argv[2]
    else:
        outfile = in_file.replace('/', '_')[:-4] + '.init'
    #input("writing to outputfile: " + outfile + ". OK?\n")
    z_zfield = {}
    z_keys = []
    z_fields = []
    for line in open(in_file, 'r').readlines():
        if len(line.split()) > 1:
            z, zfield = line.split()
            #zfield = '-' + zfield # it is not negative but should be
            z = float(z)
            #z_zfield[decimal.Decimal(z)] = decimal.Decimal(zfield)
            z_zfield[z] = zfield
            z_keys.append(z)
            z_fields.append(zfield)
    string = in_file
    bins_x = 1
    bins_y = 1
    bins_z = len(list(z_zfield.keys()))
    header = string + '\n'
    header += "##SEED##  ##EVENTS##\n"
    header += "##TURN## ##TILT## 1.0\n"
    header += "0.00 0.0 0.00\n"
    header += str(thickness) + '. '
    header += str(pitch_x) + '. '
    header += str(pitch_y) + '. '
    header += str(temperature) + '. '
    header += "0.0 1.12 1 "
    header += str(bins_x) + ' '
    header += str(bins_y) + ' '
    header += str(bins_z) + ' '
    header += '0\n'
    #z_keys = list(set(z_keys))
    z_keys = range(bins_z)
    print("number of bins: " + str(len(z_keys)))
    print("number of keys: " + str(len(z_zfield.keys())))
    with open(outfile, 'w') as efieldfile:
        efieldfile.write(header)
        for binz in range(1, bins_z + 1):
            for binx in range(1, bins_x + 1):
                for biny in range(1, bins_y + 1):
                    line = str(binx) + ' ' # bin x
                    line += str(biny) + ' ' # bin y
                    line += str(binz) + ' ' # bin z
                    line += '0.0 ' # efield in x
                    line += '0.0\t ' # efield in y
                    line += str(z_fields[binz - 1]) # efield in z
                    # line = str(binx) + '   ' # bin x
                    # line += str(biny) + '   ' # bin y
                    # line += str(binz).ljust(4) # bin z
                    # line += '0.0   ' # efield in x
                    # line += '0.0   ' # efield in y
                    #line += str(z_fields[binz - 1]).ljust(16) # efield in z
                    line += '\n'
                    efieldfile.write(line)


    print("wrote to file " + outfile)




