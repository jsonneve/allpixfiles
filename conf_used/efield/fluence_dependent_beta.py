#!/usr/bin/env python3
# cc0 copyleft -- public domain

import sys
import decimal

def beta(fluence, k1, k2):
    if fluence == 0:
        print("error: zero fluence!")
        return None
    beta = decimal.Decimal(k1) + decimal.Decimal(k2)/decimal.Decimal(fluence)
    return beta


def beta_e(fluence):
    """
    fluence dependent trapping factor for electrons
    """
    k1_e = decimal.Decimal(1.706e-16) #+- 3.097e-17
    k2_e = decimal.Decimal(0.1136) #+- 0.03805
    #beta_e = 1.706e-16 + 0.1136 / fluence
    return decimal.Decimal(beta(decimal.Decimal(fluence), k1_e, k2_e))

def beta_h(fluence):
    """
    fluence dependent trapping factor for holes
    """
    k1_h = decimal.Decimal(2.789e-16) #+- 4.627e-17
    k2_h = decimal.Decimal(0.09313) #+- 0.03805
    #beta_h = 2.789e-16 + 0.09313 / fluence
    return decimal.Decimal(beta(fluence, k1_h, k2_h))

if __name__ == "__main__":
    show_usage = False
    decimal.getcontext().prec = 5
    if len(sys.argv) > 1:
        fluence = sys.argv[1]
        try:
            fluence = float(fluence)
            print("beta for electrons:")
            print(str(beta_e(fluence)))
            print("beta for holes:")
            print(str(beta_h(fluence)))
        except:
            print("Please provide a number for the fluence")
            show_usage = True
    else:
        show_usage = True

    if show_usage:
        print("Usage:")
        print("\t./fluence_dependent_beta.py <fluence>")
        print("\t./fluence_dependent_beta.py 3e15")



