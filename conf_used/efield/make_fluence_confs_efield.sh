#!/usr/bin/env zsh
for fluenceneq in {0..10}
    do
        echo ${fluenceneq}e15
        sed s/fluenceneq/${fluenceneq}e15/g eudet_2dut_trapping_efield.conf > eudet_2dut_${fluenceneq}e15_trapping_efield.conf
    done


