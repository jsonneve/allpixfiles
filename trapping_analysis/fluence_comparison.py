#!/usr/bin/env python3
from matplotlib import pyplot as plt
import math
import os
import numpy as np
import scipy.optimize as opt
import sys

# from https://ipython-books.github.io/93-fitting-a-function-to-data-with-nonlinear-least-squares/


def trapping(fluence, time, beta, initial_charge):
    """
    Trapping function as used in allpix2.
    From Kramberger et. al.
    """
    #beta = 4.2e-16
    #fluence *= 1e15 # in this case!
    return initial_charge*np.exp(-time*fluence*beta)

if __name__ == "__main__":
    filedata = 'fluence_vs_mpv.dat'
    filedata2 = 'fluence_vs_mpv_jory4.dat'
    thickness = str(100)
    args = sys.argv
    if 'nofit' in args:
        nofit = True
        args.remove('nofit')
        print("Not fitting as requested.")
    else:
        nofit = False
        print("Fitting as requested")
    if len(args) > 2:
        filedata = args[1]
        filedata2 = args[2]
    if len(args) > 4:
        label = args[3]
        label2 = args[4]
    else:
        label = 'before'
        label2 = 'after'
    print("will use labels:")
    print(label)
    print(label2)
    if len(args) > 5:
        thickness = args[5]
    plt.figure(figsize=(15,8))
    labels = {filedata: label, filedata2: label2}
    for data in labels:
        x = []
        y = []
        yerr = []
        with open(data, 'r') as mpvs:
            for line in mpvs:
                if 'mpv' in line or line.startswith('#') or line.strip() == '': continue
                fluence, mpv, err = line.split()
                #if fluence.startswith('10'): continue
                if 'None' in mpv: continue
                x.append(float(fluence.split('e')[0]))
                y.append(float(mpv))
                yerr.append(float(err))
        if not nofit:
            (time_, beta_, initial_charge_), _ = opt.curve_fit(trapping, x[1:], y[1:]) # start from a fluence, not 0 fluence


        if '25' in data:
            size = '25x100'
        else:
            size = '50x50'
        label = labels[data]
        markerstyle = '.'
        if nofit:
            markerstyle = '*'
        plt.errorbar(x, y, yerr = yerr, label='Landau MPV from fit\n for RD53A ' + thickness + ' $\mu$m, ' + size + "$\mu$m$^2$, " + label, fmt=markerstyle, markersize=20, alpha=0.7)
        #plt.plot(x, [7/np.exp(xi*0.42) for xi in x], label='$7\cdot e^{-0.42\Phi_{eq}}$')
        if not nofit:
            plt.plot(x[1:], [trapping(xi, time_, beta_, initial_charge_) for xi in x[1:]], label="fit: $" + str(round(initial_charge_,1)) + "e^{-" + str(round(time_*beta_,2)) + " \Phi_{eq}}$ \nfor RD53A 100 $\mu$m, " + size + "$\mu$m$^2$, " + label)
    plt.title("Cluster charge prediction from Allpix$^2$ with trapping", fontsize = 24)
    plt.xlabel("Fluence $\Phi_{\mathrm{eq}}$ /[$10^{15}$]", fontsize = 18)
    plt.ylabel("Most probable value / [ke$^-$]", fontsize = 18)
    plt.legend(fontsize = 16, loc='best')
    plt.xticks(fontsize=18)
    plt.yticks(fontsize=18)
    dirname = os.path.dirname(filedata2)
    plot_file_name = os.path.basename(filedata)[:-4]
    plot_file_name2 = os.path.basename(filedata2)[:-4]
    if nofit:
        plot_file_name2 += '_nofit'
    plt.savefig(os.path.join(dirname, plot_file_name + '_' + plot_file_name2 + '.png'), bbox_inches="tight")
    plt.show()

