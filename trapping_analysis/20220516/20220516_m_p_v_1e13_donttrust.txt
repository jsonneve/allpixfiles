 % 
jsonneve@lxplus799 ~/allpix2/allpixfiles/trapping_analysis/20220516
 % for v in {1..18}; do export volt=$((50*$v)); export fluence=1e13; echo python ../mpv_histo.py /eos/user/j/jsonneve/allpix2/atlas/20220414/trapping_results_200um_diode_transient_bias_${volt}V_${fluence}.root  DetectorHistogrammer/dut/charge/cluster_charge $volt; echo  20220516_mpv_${fluence}.txt; python ../mpv_histo.py /eos/user/j/jsonneve/allpix2/atlas/20220414/trapping_results_200um_diode_transient_bias_${volt}V_${fluence}.root  DetectorHistogrammer/dut/charge/cluster_charge $volt; done                       
python ../mpv_histo.py /eos/user/j/jsonneve/allpix2/atlas/20220414/trapping_results_200um_diode_transient_bias_50V_1e13.root DetectorHistogrammer/dut/charge/cluster_charge 50
20220516_mpv_1e13.txt
Info in <TCanvas::Print>: png file trapping_results_200um_diode_transient_bias_50V_1e13_DetectorHistogrammer_dut_charge_cluster_charge_cluster_charge.png has been created
x_axis, mpv, mpv error
50 4.42 0.0275305
python ../mpv_histo.py /eos/user/j/jsonneve/allpix2/atlas/20220414/trapping_results_200um_diode_transient_bias_100V_1e13.root DetectorHistogrammer/dut/charge/cluster_charge 100
20220516_mpv_1e13.txt
Info in <TCanvas::Print>: png file trapping_results_200um_diode_transient_bias_100V_1e13_DetectorHistogrammer_dut_charge_cluster_charge_cluster_charge.png has been created
x_axis, mpv, mpv error
100 8.55298 0.0466931
python ../mpv_histo.py /eos/user/j/jsonneve/allpix2/atlas/20220414/trapping_results_200um_diode_transient_bias_150V_1e13.root DetectorHistogrammer/dut/charge/cluster_charge 150
20220516_mpv_1e13.txt
Info in <TCanvas::Print>: png file trapping_results_200um_diode_transient_bias_150V_1e13_DetectorHistogrammer_dut_charge_cluster_charge_cluster_charge.png has been created
x_axis, mpv, mpv error
150 11.8894 0.0722618
python ../mpv_histo.py /eos/user/j/jsonneve/allpix2/atlas/20220414/trapping_results_200um_diode_transient_bias_200V_1e13.root DetectorHistogrammer/dut/charge/cluster_charge 200
20220516_mpv_1e13.txt
Info in <TCanvas::Print>: png file trapping_results_200um_diode_transient_bias_200V_1e13_DetectorHistogrammer_dut_charge_cluster_charge_cluster_charge.png has been created
x_axis, mpv, mpv error
200 12.2214 0.0711767
python ../mpv_histo.py /eos/user/j/jsonneve/allpix2/atlas/20220414/trapping_results_200um_diode_transient_bias_250V_1e13.root DetectorHistogrammer/dut/charge/cluster_charge 250
20220516_mpv_1e13.txt
Info in <TCanvas::Print>: png file trapping_results_200um_diode_transient_bias_250V_1e13_DetectorHistogrammer_dut_charge_cluster_charge_cluster_charge.png has been created
x_axis, mpv, mpv error
250 12.4831 0.0731262
python ../mpv_histo.py /eos/user/j/jsonneve/allpix2/atlas/20220414/trapping_results_200um_diode_transient_bias_300V_1e13.root DetectorHistogrammer/dut/charge/cluster_charge 300
20220516_mpv_1e13.txt
Info in <TCanvas::Print>: png file trapping_results_200um_diode_transient_bias_300V_1e13_DetectorHistogrammer_dut_charge_cluster_charge_cluster_charge.png has been created
x_axis, mpv, mpv error
300 12.7071 0.065786
python ../mpv_histo.py /eos/user/j/jsonneve/allpix2/atlas/20220414/trapping_results_200um_diode_transient_bias_350V_1e13.root DetectorHistogrammer/dut/charge/cluster_charge 350
20220516_mpv_1e13.txt
Info in <TCanvas::Print>: png file trapping_results_200um_diode_transient_bias_350V_1e13_DetectorHistogrammer_dut_charge_cluster_charge_cluster_charge.png has been created
x_axis, mpv, mpv error
350 12.7582 0.0695459
python ../mpv_histo.py /eos/user/j/jsonneve/allpix2/atlas/20220414/trapping_results_200um_diode_transient_bias_400V_1e13.root DetectorHistogrammer/dut/charge/cluster_charge 400
20220516_mpv_1e13.txt
Info in <TCanvas::Print>: png file trapping_results_200um_diode_transient_bias_400V_1e13_DetectorHistogrammer_dut_charge_cluster_charge_cluster_charge.png has been created
x_axis, mpv, mpv error
400 12.8915 0.0675968
python ../mpv_histo.py /eos/user/j/jsonneve/allpix2/atlas/20220414/trapping_results_200um_diode_transient_bias_450V_1e13.root DetectorHistogrammer/dut/charge/cluster_charge 450
20220516_mpv_1e13.txt
Info in <TCanvas::Print>: png file trapping_results_200um_diode_transient_bias_450V_1e13_DetectorHistogrammer_dut_charge_cluster_charge_cluster_charge.png has been created
x_axis, mpv, mpv error
450 12.8051 0.0762091
python ../mpv_histo.py /eos/user/j/jsonneve/allpix2/atlas/20220414/trapping_results_200um_diode_transient_bias_500V_1e13.root DetectorHistogrammer/dut/charge/cluster_charge 500
20220516_mpv_1e13.txt
Info in <TCanvas::Print>: png file trapping_results_200um_diode_transient_bias_500V_1e13_DetectorHistogrammer_dut_charge_cluster_charge_cluster_charge.png has been created
x_axis, mpv, mpv error
500 12.9132 0.0715309
python ../mpv_histo.py /eos/user/j/jsonneve/allpix2/atlas/20220414/trapping_results_200um_diode_transient_bias_550V_1e13.root DetectorHistogrammer/dut/charge/cluster_charge 550
20220516_mpv_1e13.txt
Info in <TCanvas::Print>: png file trapping_results_200um_
python ../mpv_histo.py /eos/user/j/jsonneve/allpix2/atlas/20220414/trapping_results_200um_diode_transient_bias_550V_1e13.root DetectorHistogrammer/dut/charge/cluster_charge 550
20220516_mpv_1e13.txt
Info in <TCanvas::Print>: png file trapping_results_200um_diode_transient_bias_550V_1e13_DetectorHistogrammer_dut_charge_cluster_charge_cluster_charge.png has been created
x_axis, mpv, mpv error
550 12.9863 0.0812347
python ../mpv_histo.py /eos/user/j/jsonneve/allpix2/atlas/20220414/trapping_results_200um_diode_transient_bias_600V_1e13.root DetectorHistogrammer/dut/charge/cluster_charge 600
20220516_mpv_1e13.txt
Info in <TCanvas::Print>: png file trapping_results_200um_diode_transient_bias_600V_1e13_DetectorHistogrammer_dut_charge_cluster_charge_cluster_charge.png has been created
x_axis, mpv, mpv error
600 13.0895 0.0701545
python ../mpv_histo.py /eos/user/j/jsonneve/allpix2/atlas/20220414/trapping_results_200um_diode_transient_bias_650V_1e13.root DetectorHistogrammer/dut/charge/cluster_charge 650
20220516_mpv_1e13.txt
Info in <TCanvas::Print>: png file trapping_results_200um_diode_transient_bias_650V_1e13_DetectorHistogrammer_dut_charge_cluster_charge_cluster_charge.png has been created
x_axis, mpv, mpv error
650 13.0486 0.0715533
python ../mpv_histo.py /eos/user/j/jsonneve/allpix2/atlas/20220414/trapping_results_200um_diode_transient_bias_700V_1e13.root DetectorHistogrammer/dut/charge/cluster_charge  21 20220516_mpv_1e13.txt¬
700
20220516_mpv_1e13.txt
Info in <TCanvas::Print>: png file trapping_results_200um_diode_transient_bias_700V_1e13_DetectorHistogrammer_dut_charge_cluster_charge_cluster_charge.png has been created
x_axis, mpv, mpv error
700 13.0632 0.0716931
python ../mpv_histo.py /eos/user/j/jsonneve/allpix2/atlas/20220414/trapping_results_200um_diode_transient_bias_750V_1e13.root DetectorHistogrammer/dut/charge/cluster_charge  19 x_axis, mpv, mpv error¬
750
20220516_mpv_1e13.txt
Info in <TCanvas::Print>: png file trapping_results_200um_diode_transient_bias_750V_1e13_DetectorHistogrammer_dut_charge_cluster_charge_cluster_charge.png has been created
x_axis, mpv, mpv error
750 13.0758 0.0705486
python ../mpv_histo.py /eos/user/j/jsonneve/allpix2/atlas/20220414/trapping_results_200um_diode_transient_bias_800V_1e13.root DetectorHistogrammer/dut/charge/cluster_charge 800
20220516_mpv_1e13.txt
Info in <TCanvas::Print>: png file trapping_results_200um_diode_transient_bias_800V_1e13_DetectorHistogrammer_dut_charge_cluster_charge_cluster_charge.png has been created
x_axis, mpv, mpv error
800 13.051 0.0751914
python ../mpv_histo.py /eos/user/j/jsonneve/allpix2/atlas/20220414/trapping_results_200um_diode_transient_bias_850V_1e13.root DetectorHistogrammer/dut/charge/cluster_charge 850
20220516_mpv_1e13.txt
Info in <TCanvas::Print>: png file trapping_results_200um_diode_transient_bias_850V_1e13_DetectorHistogrammer_dut_charge_cluster_charge_cluster_charge.png has been created
x_axis, mpv, mpv error
850 13.1327 0.0713215
python ../mpv_histo.py /eos/user/j/jsonneve/allpix2/atlas/20220414/trapping_results_200um_diode_transient_bias_900V_1e13.root DetectorHistogrammer/dut/charge/cluster_charge 900
20220516_mpv_1e13.txt
Info in <TCanvas::Print>: png file trapping_results_200um_diode_transient_bias_900V_1e13_DetectorHistogrammer_dut_charge_cluster_charge_cluster_charge.png has been created
x_axis, mpv, mpv error
900 13.0341 0.0701075
