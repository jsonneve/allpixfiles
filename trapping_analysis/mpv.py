# cc0 copyleft -- public domain
#!/usr/bin/env python3
import sys
import os

def landau_peak_mpv(rootfile):
    """
    Get Landau peak: most probable.
    .x fitlang.C("clqB4")
    peak and most probable value ("peak" from fit)
    """
    # print('will run')
    peak = mpv = mpv_err = None
    hist = "DetectorHistogrammer/dut_0/cluster_charge"
    command = './landau_peak.sh ' +  rootfile + ' ' + hist + ' > temp'
    # print(command)
    command_out = os.system(command)
    with open('temp', 'r') as landaupeak:
        for line in landaupeak:
            if 'peak at' in line:
                peak = float(line.split()[2])
            elif '1  peak' in line:
                mpv = float(line.split()[2])
                mpv_err = float(line.split()[3])
    return peak, mpv, mpv_err


if __name__ == "__main__":

    if len(sys.argv) < 2:
        print("Usage: ")
        print("python mpv.py rootfile.root")
        sys.exit()
    rootfile = sys.argv[1]
    peak, mpv, mpv_err = landau_peak_mpv(rootfile)
    if len(sys.argv) > 2:
        x_axis = sys.argv[2]
        print ("x_axis, mpv, mpv error")
        print(x_axis, mpv, mpv_err)
    else:
        print ("peak, mpv, mpv error")
        print(peak, mpv, mpv_err)




