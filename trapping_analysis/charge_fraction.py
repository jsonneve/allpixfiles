# cc0 copyleft -- public domain
#!/usr/bin/env python3
import sys
import os

def charge_fraction(rootfile, hist=None, histname=None):
    """
    Get mean from histogram.
    """
    mean = stddev = None
    if histname == None:
        histname = hist
    if '/' in histname: histname = histname.replace('/','_')
    # otherwise take hist name as it is
    command = './mean_fraction.sh ' +  rootfile + ' ' + hist + ' ' + histname + ' > temp'
    # print(command)
    command_out = os.system(command)
    ndoubles = 0
    with open('temp', 'r') as meantxt:
        for line in meantxt:
            if 'double' in line:
                if ndoubles == 0: # first mean is printed
                    mean = float(line.split()[1])
                if ndoubles == 1: # second stddev is printed
                    stddev = float(line.split()[1])
                ndoubles += 1
    return mean, stddev


if __name__ == "__main__":

    if len(sys.argv) < 2:
        print("Usage: ")
        print("python charge_fraction.py rootfile.root [histogram name] [x-axis number]")
        sys.exit()
    rootfile = sys.argv[1]
    if len(sys.argv) > 2:
        histname = sys.argv[2]
    else:
        histname = None
    mean, stddev = charge_fraction(rootfile, histname)
    if len(sys.argv) > 3:
        x_axis = sys.argv[3]
        print ("x_axis, mean, stddev")
        print(str(x_axis) + '    ' + str( mean) + '    ' + str(stddev))
    else:
        print ("mean, stddev")
        print(str(peak) + '    ' + str(mean) + '    ' + str(stddev))




