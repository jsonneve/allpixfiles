#!usr/bin/env python3
import numpy as np
import math
import matplotlib as mpl
mpl.use('Agg')
from matplotlib import pyplot as plt
import glob
import subprocess as sup
import sys
import os
import ROOT as rt


def resolution_daniel(rootfile, dut=True):
    """
    given a root file get the right histogram and determine the resolution from the RMS
    """
    rtfile = rt.TFile(rootfile)
    keys = dict_of_keys(rootfile)
    if dut:
        # dut resolution
        hist = 'cmsdyfcq4nod'
        if hist not in keys:
           hist = "dutdx"
    else:
        hist = "sixdycsi"
        if hist not in keys:
           hist = "sixdyc"

    res, res_err = None, None
    if hist in keys:
        print('will run')
        command = './rms.sh ' +  rootfile + ' ' + hist + ' > temp'
        print(command)
        print(['./rms.sh', rootfile + ' ' + hist])
        #res_out = sup.check_output(['./rms.sh', rootfile + ' ' + hist + ' > temp'])
        res_out = os.system(command)
        #print res_out
        with open('temp', 'r') as rmsfit:
            for line in rmsfit:
                if 'rms3' in line:
                    res = float(line.split()[-1])
                    res_err = 0 #?
        if not dut and not res == None:
            res = res/2.
            res_err = res_err/4.

    return res, res_err

def mean(h, verbose=False):
    """
    Get mean from 90 %  of data
    >>> import ROOT
    >>> import ROOT as rt
    >>> t = rt.TFile("scopem37234.root")
    >>> s = t.Get("sixdxc")
    >>> s
    <ROOT.TH1I object ("sixdxc") at 0x3e1e8e0>
    >>> s.GetMean()
    0.0015987816815981855
    >>> s.GetMeanError()
    3.3543239843231e-05
    >>> s.GetXaxis().SetRangeUser(-0.02, 0.02)
    >>> s.GetMeanError()
    1.1546577018878744e-05
    >>> s.GetMean()
    0.0009318663663825383
    >>> s.GetRMS()
    0.008107053923208643
    >>> s.GetRMSError()
    8.164662909541911e-06
    >>> 

    """
    mean = h.GetMean()
    meanerr = h.GetMeanError()
    return mean, meanerr


def rms(h, verbose=False):
    """
    Get RMS from 90 %  of data
    >>> import ROOT
    >>> import ROOT as rt
    >>> t = rt.TFile("scopem37234.root")
    >>> s = t.Get("sixdxc")
    >>> s
    <ROOT.TH1I object ("sixdxc") at 0x3e1e8e0>
    >>> s.GetMean()
    0.0015987816815981855
    >>> s.GetMeanError()
    3.3543239843231e-05
    >>> s.GetXaxis().SetRangeUser(-0.02, 0.02)
    >>> s.GetMeanError()
    1.1546577018878744e-05
    >>> s.GetMean()
    0.0009318663663825383
    >>> s.GetRMS()
    0.008107053923208643
    >>> s.GetRMSError()
    8.164662909541911e-06
    >>> 

    """
    rms = h.GetRMS()
    rmserr = h.GetRMSError()
    try_bounds = None
    #if verbose:
    #    print('original rms', rms)
    #try_bounds = 0.02
    #try_bounds = 1.08
    #while True:
    #    h.GetXaxis().SetRangeUser(-try_bounds, try_bounds)
    #    rms = h.GetRMS()
    #    rmserr = h.GetRMSError()
    #    if verbose:
    #        print('rms', rms)
    #        print('bounds', try_bounds)
    #    h.GetXaxis().SetRangeUser(-try_bounds, try_bounds)
    #    if try_bounds*0.95 < rms*2.5 < try_bounds*1.05:
    #        break
    #    else:
    #        if rms*2.5 < try_bounds:
    #            try_bounds = try_bounds * 0.9
    #        else:
    #            try_bounds = try_bounds * 1.1
    return rms, rmserr, try_bounds


def dict_of_keys(rootfilename):
    """
    Return a dictionary of keys sorted by keyname
    >>> str(keys[100])
    'Name: nrow7 Title: 7 cluster size y'
    """
    rtfile = rt.TFile(rootfilename)
    keys = rtfile.GetListOfKeys()
    keys_names = {}
    for key in keys:
        keyname, keytitle = str(key).replace("Name:", '').strip().split("Title: ")
        keys_names[keyname.strip()] = {'key': key, 'name': keyname.strip(), 'title': keytitle.strip()}
    return keys_names


if __name__ == "__main__":

    if len(sys.argv) < 3:
        print("Usage:")
        print("\tpython3 trapping.py rootfile histname")
        print("Example:")
        sys.exit()


    rootfile = sys.argv[1]
    histo = sys.argv[2]
    #print("Opening file " + rootfile)
    rtfile = rt.TFile(rootfile)
    #print("Looking at histogram " + histo)
    if "ljubljana" in rootfile or "cmstracker" in rootfile:
        if 'generic' in rootfile:
            fluences = rootfile[rootfile.index('00um_'):rootfile.index('_generic')]
        else:
            fluences = rootfile[rootfile.index('00um_'):rootfile.index('_transient')]
        if 'detector1' in histo:
            fluence = fluences.split('_')[-2]
        else:
            fluence = fluences.split('_')[-1]
    else:
        fluence = ''
    histo = rtfile.Get(histo)
    #rms, rmserr, bounds = rms(histo)
    #print("RMS for bounds " + str(bounds) + " is " + str(rms) + " +- " + str(rmserr))
    mean, meanerr = mean(histo)
    #print("mean is " + str(mean) + " +- " + str(meanerr))
    print(fluence + "\t" + str(mean) + "\t+-\t" + str(meanerr))


