#!/usr/bin/env zsh
if [[ -n $1 ]]
then
    rootfile=$1
else
    echo No root file given!
    echo Usage:
    echo "   ./landau_peak.sh rootfile [histo]"
    exit
fi
if [[ -n $2 ]]
then
    histo=$2
else
    histo="DetectorHistogrammer/dut_0/cluster_charge"
fi
if [[ -n $3 ]]
then
    histname=$(echo ${3}_$(basename $histo))
else
    histname=$(basename $histo)
fi
rootfilename=$(basename ${rootfile})
filename="${rootfilename%.*}"
echo $filename
pngfilename=${filename}.png
pngfilename=${filename}_${histname}.png
echo will save png to $pngfilename




root -b -l ${rootfile} << EOF
gSystem->AddIncludePath(" -I/home/wave/allpix2/rootware")
.x cvsq.C
TH1 *h = (TH1*)gDirectory->Get( "${histo}" )
h->Draw()
gStyle->SetStatX(8.8);
gStyle->SetStatY(8.8);
c1->SaveAs("${pngfilename}");
.q
EOF


