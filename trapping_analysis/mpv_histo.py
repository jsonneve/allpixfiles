# cc0 copyleft -- public domain
#!/usr/bin/env python3
import sys
import os

def landau_peak_mpv(rootfile, hist=None, histname=None):
    """
    Get Landau peak: most probable.
    .x fitlang.C("clqB4")
    peak and most probable value ("peak" from fit)
    """
    # print('will run')
    peak = mpv = mpv_err = None
    if histname == None:
        histname = hist
    if hist == None or hist == '50x50':
        hist = "DetectorHistogrammer/dut_0/cluster_charge" # 50x50
    elif hist == '25x100':
        hist = "DetectorHistogrammer/dut_1/cluster_charge" # 25x100
    if '/' in histname: histname = histname.replace('/','_')
    # otherwise take hist name as it is
    command = ' ~/allpix2/allpixfiles/trapping_analysis/landau_peak.sh ' +  rootfile + ' ' + hist + ' ' + histname + ' > temp'
    # print(command)
    command_out = os.system(command)
    with open('temp', 'r') as landaupeak:
        for line in landaupeak:
            if 'peak at' in line:
                peak = float(line.split()[2])
            elif '1  peak' in line:
                mpv = float(line.split()[2])
                mpv_err = float(line.split()[3])
    return peak, mpv, mpv_err


if __name__ == "__main__":

    if len(sys.argv) < 2:
        print("Usage: ")
        print("python mpv.py rootfile.root [histogram name] [x-axis number]")
        sys.exit()
    rootfile = sys.argv[1]
    if len(sys.argv) > 2:
        histname = sys.argv[2]
    else:
        histname = None
    peak, mpv, mpv_err = landau_peak_mpv(rootfile, histname)
    if len(sys.argv) > 3:
        x_axis = sys.argv[3]
        print ("x_axis, mpv, mpv error")
        print(x_axis, mpv, mpv_err)
    else:
        print ("peak, mpv, mpv error")
        print(peak, mpv, mpv_err)




