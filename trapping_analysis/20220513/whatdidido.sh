# Source good version of allpix^2
source /cvmfs/clicdp.cern.ch/software/allpix-squared/latest/x86_64-centos7-clang12-opt/setup.sh

# Get MPV
for v in {1..18}; do export volt=$((50*$v)); fluence=1e13; python ../mpv_histo.py /eos/user/j/jsonneve/allpix2/atlas/20220414/trapping_results_200um_diode_transient_bias_${volt}V_${fluence}.root  DetectorHistogrammer/dut/charge/cluster_charge $volt; done > 20220513_mpv_${fluence}.txt

# Analyze data to get charge fractions
for v in {1..18}; do export volt=$((50*$v)); fluence=1e13; ../analyze_allpix.sh /eos/user/j/jsonneve/allpix2/atlas/20220414/trapping_data_200um_diode_transient_bias_${volt}V_${fluence}.root /eos/user/j/jsonneve/allpix2/atlas/20220513/trapping_outputcharge_200um_diode_transient_bias_${volt}V_${fluence}.root; done

# Get mean charges
for v in {1..18}; do export volt=$((50*$v)); fluence=1e13; python ../charge_fraction.py /eos/user/j/jsonneve/allpix2/atlas/20220513/trapping_outputcharge_200um_diode_transient_bias_${volt}V_${fluence}.root charge_fraction $volt; done  > 20220513_charge_fraction_${fluence}.txt

# Get MPV
for v in {1..18}; do export volt=$((50*$v)); fluence=1e15; python ../mpv_histo.py /eos/user/j/jsonneve/allpix2/atlas/20220414/trapping_results_200um_diode_transient_bias_${volt}V_${fluence}.root  DetectorHistogrammer/dut/charge/cluster_charge $volt; done > 20220513_mpv_${fluence}.txt

# Analyze data to get charge fractions
for v in {1..18}; do export volt=$((50*$v)); fluence=1e15; ../analyze_allpix.sh /eos/user/j/jsonneve/allpix2/atlas/20220414/trapping_data_200um_diode_transient_bias_${volt}V_${fluence}.root /eos/user/j/jsonneve/allpix2/atlas/20220513/trapping_outputcharge_200um_diode_transient_bias_${volt}V_${fluence}.root; done

# Get mean charges
for v in {1..18}; do export volt=$((50*$v)); fluence=1e15; python ../charge_fraction.py /eos/user/j/jsonneve/allpix2/atlas/20220513/trapping_outputcharge_200um_diode_transient_bias_${volt}V_${fluence}.root charge_fraction $volt; done  > 20220513_charge_fraction_${fluence}.txt
