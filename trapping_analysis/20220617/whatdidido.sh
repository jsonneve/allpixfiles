# Source good version of allpix^2
source /cvmfs/clicdp.cern.ch/software/allpix-squared/latest/x86_64-centos7-clang12-opt/setup.sh

# Get MPV
for v in {1..18}; do export volt=$((50*$v)); fluence=3e14; python ../mpv_histo.py /eos/user/j/jsonneve/allpix2/atlas/20220617/trapping_results_200um_diode_transient_bias_${volt}V_${fluence}_init_from_plx.root  DetectorHistogrammer/dut/charge/cluster_charge $volt; done > 20220617_mpv_${fluence}.txt
#    /eos/user/j/jsonneve/allpix2/atlas/20220617/trapping_data_200um_diode_transient_bias_350V_3e14_init_from_plx.root

# Analyze data to get charge fractions
for v in {1..18}; do export volt=$((50*$v)); fluence=3e14; ../analyze_allpix.sh /eos/user/j/jsonneve/allpix2/atlas/20220617/trapping_data_200um_diode_transient_bias_${volt}V_${fluence}_init_from_plx.root /eos/user/j/jsonneve/allpix2/atlas/20220617/trapping_outputcharge_200um_diode_transient_bias_${volt}V_${fluence}_init_from_plx.root; done

# Get mean charges
for v in {1..18}; do export volt=$((50*$v)); fluence=3e14; python ../charge_fraction.py /eos/user/j/jsonneve/allpix2/atlas/20220617/trapping_outputcharge_200um_diode_transient_bias_${volt}V_${fluence}_init_from_plx.root charge_fraction $volt; done  > 20220617_charge_fraction_${fluence}.txt

