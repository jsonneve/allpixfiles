import ROOT as rt
import datetime
import sys
import os

def get_objects_from_directories(rootfileobject, key, prefix='', objects=[]):
    """
    Go through ROOT file and get objects from directories.
    Start with the directory 'key'.
    Append all objects found to ojects.
    Keep track of the total path with prefix.
    """
    if key == None: #.IsZombie():
        # skip key
        return objects
    if not key.IsFolder():
        objects.append(key)
        return objects
    else:
        keyname = key.GetTitle()
        #print 'keyname:', keyname
        dirname = os.path.join(prefix, keyname)
        directory = rootfileobject.GetDirectory(dirname)
        #print 'dirname:', dirname
        listofkeys = directory.GetListOfKeys()
        for key in listofkeys:
            #print 'key in list of keys', key
            prefix = dirname #os.path.join(dirname, key.GetTitle())
            #print 'prefix', prefix
            objects = get_objects_from_directories(rootfileobject, key, prefix=prefix, objects=objects)
    return objects

if __name__ == "__main__":
    args = sys.argv
    if len(args) < 2: 
        print("usage: python3 list_of_keys.py rootfilename")
        sys.exit()
    filepath = args[1]
    t = rt.TFile(filepath)
    print("Now printing ls")
    #print(t.ls())
    print("Done printing ls")
    maindirs = []
    keys = t.GetListOfKeys()
    dirs = t.ls()
    for key in keys:
        keyname = key.GetName().split()[-1]
        print(keyname)
        maindirs.append(keyname)
    for maindir in maindirs:
        print('')
        print(maindir)
        d = t.GetDirectory(maindir)
        listofkeys = get_objects_from_directories(t, d)
        #print( listofkeys)

        for key in listofkeys:

            if key == None: continue # not present
            name = key.GetName()
            print(name)
            canv = key.ReadObj()
            try:
                canvasobjects = canv.GetListOfPrimitives()
                for ob in canvasobjects:
                    graphname = ob.GetName()
                    print(graphname)
                graph = canv.FindObject(graphname)


                maxy = rt.TMath.MaxElement(graph.GetN(), graph.GetY())
                title = graph.GetTitle()
                print("title")
            except:
                pass
                #print("Could not get list of primitives")
        print("")
