#!/usr/bin/env zsh
if [[ -n $1 ]]
then
    rootfile=$1
else
    echo No root file given!
    echo Usage:
    echo "   ./analyze_allpix.sh rootfile outputrootfile"
    exit
fi
if [[ -n $2 ]]
then
    outputrootfile=$2
else
    filename="${rootfilename%.*}"
    outputrootfile=${filename}_analyzed.root
fi
echo writing to $outputrootfile
#chargepath=$(readlink -e ../charge.C)++


root -b -l ${rootfile} << EOF
.L ../libAllpixObjects.so
.L ../charge.C++
auto file = new TFile("${outputrootfile}", "RECREATE")
auto tree = charge(_file0, "dut")
tree->Write()
.q
EOF


