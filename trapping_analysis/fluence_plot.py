#!/usr/bin/env python3
from matplotlib import pyplot as plt
import math
import numpy as np
import scipy.optimize as opt
import sys

# from https://ipython-books.github.io/93-fitting-a-function-to-data-with-nonlinear-least-squares/


def trapping(fluence, time, beta, initial_charge):
    """
    Trapping function as used in allpix2.
    From Kramberger et. al.
    """
    #beta = 4.2e-16
    #fluence *= 1e15 # in this case!
    return initial_charge*np.exp(-time*fluence*beta)

if __name__ == "__main__":
    data = 'fluence_vs_mpv.dat'
    if len(sys.argv) > 1:
        data = sys.argv[1]
    x = []
    y = []
    yerr = []
    with open(data, 'r') as mpvs:
        for line in mpvs:
            fluence, mpv, err = line.split()
            if 'None' in mpv: continue
            #if fluence.startswith('10'): continue
            x.append(float(fluence.split('e')[0]))
            y.append(float(mpv))
            yerr.append(float(err))
    (time_, beta_, initial_charge_), _ = opt.curve_fit(trapping, x, y)

    plt.figure(figsize=(15,8))
    plt.errorbar(x, y, yerr = yerr, label='Landau MPV from fit', fmt=".")
    #plt.plot(x, [7/np.exp(xi*0.42) for xi in x], label='$7\cdot e^{-0.42\Phi_{eq}}$')
    plt.plot(x, [trapping(xi, time_, beta_, initial_charge_) for xi in x], label="fit: $" + str(round(initial_charge_,1)) + "e^{-" + str(round(time_*beta_,2)) + " \Phi_{eq}}$")
    if '25' in data:
        size = '25x100'
    else:
        size = '50x50'
    plt.title("RD53A 100 $\mu$m, 25x100  $\mu$m$^2$, cluster charge prediction from Allpix$^2$ with trapping", fontsize = 24)
    plt.xlabel("Fluence $\Phi_{\mathrm{eq}}$ /[$10^{15}$]", fontsize = 18)
    plt.ylabel("Most probable value / [ke$^-$]", fontsize = 18)
    plt.legend(fontsize = 18)
    plt.xticks(fontsize=18)
    plt.yticks(fontsize=18)
    plot_file = data[:-3] + 'png'
    plt.savefig(plot_file, bbox_inches="tight")
    plt.show()

