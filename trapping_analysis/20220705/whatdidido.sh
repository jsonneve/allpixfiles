# Source good version of allpix^2
source /cvmfs/clicdp.cern.ch/software/allpix-squared/latest/x86_64-centos7-clang12-opt/setup.sh

# Get MPV
fluence=10e14
for v in {1..18}; do export volt=$((50*$v)); python ../mpv_histo.py /eos/user/j/jsonneve/allpix2/atlas/20220705/trapping_results_200um_diode_transient_bias_${volt}V_${fluence}_apf_from_grd.root  DetectorHistogrammer/dut/charge/cluster_charge $volt; done > 20220705_mpv_${fluence}.txt
#    /eos/user/j/jsonneve/allpix2/atlas/20220705/trapping_data_200um_diode_transient_bias_350V_15e14_apf_from_grd.root

# Analyze data to get charge fractions
for v in {1..18}; do export volt=$((50*$v)); ../analyze_allpix.sh /eos/user/j/jsonneve/allpix2/atlas/20220705/trapping_data_200um_diode_transient_bias_${volt}V_${fluence}_apf_from_grd.root /eos/user/j/jsonneve/allpix2/atlas/20220705/trapping_outputcharge_200um_diode_transient_bias_${volt}V_${fluence}_apf_from_grd.root; done

# Get mean charges
for v in {1..18}; do export volt=$((50*$v)); python ../charge_fraction.py /eos/user/j/jsonneve/allpix2/atlas/20220705/trapping_outputcharge_200um_diode_transient_bias_${volt}V_${fluence}_apf_from_grd.root charge_fraction $volt; done  > 20220705_charge_fraction_${fluence}.txt

# Get MPV
for fluence in 3e14 10e14 15e14; for v in {1..18}; do export volt=$((50*$v)); python ../mpv_histo.py /eos/user/j/jsonneve/allpix2/atlas/20220705/trapping_results_200um_diode_transient_bias_${volt}V_${fluence}_apf_from_grd.root  DetectorHistogrammer/dut/charge/cluster_charge $volt; done > 20220705_mpv_${fluence}.txt
#    /eos/user/j/jsonneve/allpix2/atlas/20220705/trapping_data_200um_diode_transient_bias_350V_15e14_apf_from_grd.root

# Analyze data to get charge fractions
for fluence in 3e14 10e14 15e14; for v in {1..18}; do export volt=$((50*$v)); ../analyze_allpix.sh /eos/user/j/jsonneve/allpix2/atlas/20220705/trapping_data_200um_diode_transient_bias_${volt}V_${fluence}_apf_from_grd.root /eos/user/j/jsonneve/allpix2/atlas/20220705/trapping_outputcharge_200um_diode_transient_bias_${volt}V_${fluence}_apf_from_grd.root; done

# Get mean charges
for fluence in 3e14 10e14 15e14; for v in {1..18}; do export volt=$((50*$v)); python ../charge_fraction.py /eos/user/j/jsonneve/allpix2/atlas/20220705/trapping_outputcharge_200um_diode_transient_bias_${volt}V_${fluence}_apf_from_grd.root charge_fraction $volt; done  > 20220705_charge_fraction_${fluence}.txt

